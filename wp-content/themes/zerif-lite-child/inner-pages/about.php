<?php
$about_page = get_page_by_path('about');
$post_id = $about_page->ID;
$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'single-post-thumbnail' );
?>
<div class="row post-row">
    <div class="col-md-12">
        <div class="page-image-wrap">
            <?php
            if(is_array($image)){
                ?>
                <figure class="page-bg-image border-gray-sm border-round" style="background-image: url('<?php echo $image[0]?>')">
                <img class="img-responsive" src="">
                </figure>
                <?php
            }
            ?>
        </div>
    </div>
    <div class="col-md-12">
        <div class="page-content">
            <?php echo $about_page->post_content;?>
        </div>
    </div>
</div>