<div class="row post-row">
<?php
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array( 'category_name' => 'our-services', 'posts_per_page' => 20, 'paged' => $paged );
$wp_query = new WP_Query($args);
while ( have_posts() ) :
    the_post();
    $post_id = get_the_ID();
    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'single-post-thumbnail' );
    $service_metas = get_post_meta($post_id);
    ?>
    <div class="col-md-3 services-content text-center">
        <div class="main-icon text-center text-green"><i class="<?php echo $service_metas['main-icon'][0] ?>"></i></div>
        <div class="title-lg service-title text-green"><?php echo the_title();?></div>
        <div class="desc-sm service-desc"><?php echo the_excerpt();?></div>
        <div class="readmore">
            <a class="text-green" href="<?php the_permalink($post_id);?>">
                Read More <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    
    <?php
endwhile;
?>
</div>

<!-- then the pagination links -->
<div class="pagination">
    <?php
    // previous_posts_link( '&larr; Newer posts' );
    // next_posts_link( ' Older posts &rarr;', $wp_query ->max_num_pages);
    echo paginate_links();
    ?>
</div>