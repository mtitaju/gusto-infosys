<?php
$about_page = get_page_by_path('contact');
$post_id = $about_page->ID;
$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'single-post-thumbnail' );
$post_metas = get_post_meta($post_id);
?>
<div class="row">
    
    <div class="col-md-6 no-padding-left no-padding-right">
        <div class="contact-map" style="background-image:url('<?php echo $image[0]?>')">
        </div>
    </div>
    <div class="col-md-6">
        <div class="page-content contact-content">
            <div class="contact-page-subtitle  text-green text-left"><?php echo $about_page->post_content;?></div>
            <div class="contact-line text-left"><span class="custom-label"> Contact No.: </span><?php echo $post_metas['contact'][0] ?> &nbsp; <i class="fa fa-phone"></i></div>
            <div class="contact-line text-left"><span class="custom-label"> Email: </span><?php echo $post_metas['email'][0] ?> &nbsp; <i class="fa fa-envelope"></i></div>
            <div class="contact-line text-left"><span class="custom-label"> Location: </span><?php echo $post_metas['location'][0] ?> &nbsp; <i class="fa fa-map-marker"></i></div>
        </div>
    </div>
</div>