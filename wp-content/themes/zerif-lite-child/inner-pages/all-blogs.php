<?php
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array( 'category_name' => 'blog', 'posts_per_page' => 2, 'paged' => $paged );
$wp_query = new WP_Query($args);
while ( have_posts() ) :
the_post();
?>
<div class="row post-row">
    <div class="col-md-3">
        <?php
        $post_id = get_the_ID();
        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'single-post-thumbnail' );
        ?>
        <div class="border-gray-sm border-round">
            <figure>
                <?php
                if(is_array($image)){
                    ?>
                    <img class="img-responsive" src="<?php echo $image[0]?>">
                    <?php
                }else{
                    ?>
                    <span class="dummy-image"><i class="fa fa-picture-o"></i></span>
                    <?php
                }
                ?>
            </figure>
        </div>
    </div>
    <div class="col-md-9">
        <h2 class="no-top-margin text-green"><?php the_title(); ?></h2>
        <div class="post-excerpt">
        <?php echo excerpt(200);?>
        </div>
    </div>
</div>
<?php
endwhile;
?>

<!-- then the pagination links -->
<div class="pagination">
    <?php
    // previous_posts_link( '&larr; Newer posts' );
    // next_posts_link( ' Older posts &rarr;', $wp_query ->max_num_pages);
    echo paginate_links();
    ?>
</div>