<?php
the_post();
global $post;
$post_id = $post->ID;
$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'single-post-thumbnail' );
$service_metas = get_post_meta($post_id);
?>
<div class="row post-row">
    <div class="col-md-12">
        <div class="page-image-wrap">
            <?php
                if(is_array($image)){
                    ?>
                    <figure class="border-gray-sm border-round">
                        <img class="img-responsive" src="<?php echo $image[0]?>">
                    </figure>
                    <?php
                }
                ?>
        </div>
    </div>
    <div class="col-md-12">
        <h2 class="text-green"><?php echo the_title();?></h2>
    </div>
    <div class="col-md-12">
        <div class="page-content">
            <?php the_content();?>
        </div>
    </div>
</div>