<?php
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array( 'category_name' => 'testimonials', 'posts_per_page' => 20, 'paged' => $paged );
$wp_query = new WP_Query($args);
while ( have_posts() ) :
the_post();
?>
<div class="row post-row">
    <div class="col-md-2">
        <?php
        $post_id = get_the_ID();
        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'single-post-thumbnail' );
        ?>
        <div class="border-green-lg border-circle">
            <figure>
                <?php
                if(is_array($image)){
                    ?>
                    <img class="img-circle img-responsive" src="<?php echo $image[0]?>">
                    <?php
                }else{
                    ?>
                    <span class="img-circle dummy-image"><i class="fa fa-picture-o"></i></span>
                    <?php
                }
                ?>
            </figure>
        </div>
    </div>
    <div class="col-md-10">
        <h2 class="no-top-margin text-green"><?php the_title(); ?></h2>
        <div class="post-excerpt text-grey">
            <?php the_excerpt();?>
        </div>
        <div class="post-content">
            <?php the_content();?>
        </div>
    </div>
</div>
<?php
endwhile;
?>

<!-- then the pagination links -->
<div class="pagination">
    <?php
    // previous_posts_link( '&larr; Newer posts' );
    // next_posts_link( ' Older posts &rarr;', $wp_query ->max_num_pages);
    echo paginate_links();
    ?>
</div>