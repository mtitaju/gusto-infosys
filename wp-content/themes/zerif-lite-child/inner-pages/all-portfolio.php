<div class="row post-row">
<?php
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array( 'category_name' => 'portfolio', 'posts_per_page' => 20, 'paged' => $paged );
$wp_query = new WP_Query($args);
while ( have_posts() ) :
    the_post();
    $post_id = get_the_ID();
    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'single-post-thumbnail' );
    $portfolio_metas = get_post_meta($post_id);
    ?>
    <div class="col-md-3 portfolio-content">
        <div class="portfolio-image-wrap">
            <figure>
                <img class="img-responsive" src="<?php echo $image[0]?>">
            </figure>
        </div>
        <div class="title-md portfolio-title text-green text-center"><?php the_title();?></div>
        <div class="desc-sm portfolio-desc"><?php the_excerpt();?></div>
        <div class="readmore text-center">
            <a href="<?php the_permalink($post_id);?>">
                Project Overview <i class="fa fa-angle-double-right"></i>
            </a>
        </div>
    </div>
    <?php
endwhile;
?>
</div>

<!-- then the pagination links -->
<div class="pagination">
    <?php
    // previous_posts_link( '&larr; Newer posts' );
    // next_posts_link( ' Older posts &rarr;', $wp_query ->max_num_pages);
    echo paginate_links();
    ?>
</div>