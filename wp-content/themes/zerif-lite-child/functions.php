<?php
function load_styles() {
	
    // enqueue parent styles
	wp_enqueue_style('zerif-lite', get_template_directory_uri() .'/style.css');
	
}
add_action('wp_enqueue_scripts', 'load_styles');

function remove_class_function( $classes ) {
	
    
    return $classes;

}

/**
 * Filter the except length to 20 words.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
function wpdocs_custom_excerpt_length( $length ) {
    global $post;
    return 90;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

function excerpt($limit) {
    $excerpt = explode(' ', get_the_excerpt(), $limit);

    if (count($excerpt) >= $limit) {
        array_pop($excerpt);
        $excerpt = implode(" ", $excerpt) . '...';
    } else {
        $excerpt = implode(" ", $excerpt);
    }

    $excerpt = preg_replace('`\[[^\]]*\]`', '', $excerpt);

    return $excerpt;
}

function custom_excerpt_link( $more ) {
	if ( is_admin() ) {
		return $more;
	}

	// Change text, make it link, and return change
	return '&hellip; <br> <div class="btn-wrapper"><a class="btn-clear btn-clear-green" href="' . get_the_permalink() . '" class="custom_excerpt_link">Read More</a></div>';
 }
 add_filter( 'excerpt_more', 'custom_excerpt_link', 999 );

?>