<?php
/**
 * Frontpage content
 *
 * @package zerif-lite
 */

$zerif_bigtitle_show = get_theme_mod( 'zerif_bigtitle_show' );

if ( ( isset( $zerif_bigtitle_show ) && $zerif_bigtitle_show != 1 ) || is_customize_preview() ) {

	get_template_part( 'sections/big_title' );

}

?>

</header> <!-- / END HOME SECTION  -->
<?php zerif_after_header_trigger(); ?>
<div id="content" class="site-content">

<?php

/* ABOUT US */

$zerif_aboutus_show = get_theme_mod( 'zerif_aboutus_show' );

if ( ( isset( $zerif_aboutus_show ) && $zerif_aboutus_show != 1 ) || is_customize_preview() ) {

get_template_part( 'sections/about_us' );

}
/* OUR FOCUS SECTION */

$zerif_ourfocus_show = get_theme_mod( 'zerif_ourfocus_show' );

if ( ( isset( $zerif_ourfocus_show ) && $zerif_ourfocus_show != 1 ) || is_customize_preview() ) {

	get_template_part( 'sections/our_focus' );

}


/* OUR PORTFOLIO */
get_template_part('sections/our_portfolio');

/* TESTIMONIALS */

$zerif_testimonials_show = get_theme_mod( 'zerif_testimonials_show' );

if ( ( isset( $zerif_testimonials_show ) && $zerif_testimonials_show != 1 ) || is_customize_preview() ) {

	get_template_part( 'sections/testimonials' );

}


/* BLOG SECTION*/

get_template_part( 'sections/blog' );