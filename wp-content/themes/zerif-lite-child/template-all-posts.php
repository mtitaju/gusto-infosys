<?php
/**
 * Template Name: All Post List
 *
 * @package zerif-lite
 */
global $post;
$post_slug = $post->post_name;
get_header();
?>

<div class="clear"></div>

</header> <!-- / END HOME SECTION  -->
<?php zerif_after_header_trigger(); ?>
<div id="top-banner-all-page" class="transparent-dark-bg">
    <div class="page-title-main-bg">
        <h1 class="text-center text-white page-title-main"><?php echo get_the_title();?></h1>
    </div>
</div>
<div id="content" class="site-content main-site-container inner-page-container">
    <div class="container">
        <?php
        switch ($post_slug) {
            case 'blog':
                get_template_part( 'inner-pages/all-blogs');
                break;
            case 'our-services':
                get_template_part( 'inner-pages/all-services');
                break;
            case 'portfolio':
                get_template_part( 'inner-pages/all-portfolio');
                break;
            case 'testimonial':
                get_template_part( 'inner-pages/all-testimonials');
                break;
            case 'contact':
                get_template_part( 'inner-pages/contact');
                break;
            case 'about':
                get_template_part( 'inner-pages/about');
                break;
        }
        ?>
    </div>
</div>

<?php

get_footer();
?>