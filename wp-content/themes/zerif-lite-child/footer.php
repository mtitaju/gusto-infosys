<?php
/**
 * The template for displaying the footer.
 * Contains the closing of the #content div and all content after
 *
 * @package zerif-lite
 */

?>

</div><!-- .site-content -->

<?php zerif_before_footer_trigger(); ?>

<footer id="footer-top" itemscope="itemscope" itemtype="http://schema.org/WPFooter">

	<?php zerif_footer_widgets_trigger(); ?>

	<div class="container">
        <div class="row footer-top">
            <div class="col-md-4 footer-col">
                <div class="footer-icon">
                    <i class="fa fa-map-marker"></i>
                </div>
                <div class="footer-desc">
                    <p>Katunje, Suryabinayak - 5, Bhaktapur</p>
                </div>
            </div>
            <div class="col-md-4 footer-col">
                <div class="footer-icon">
                    <i class="fa fa-envelope"></i>
                </div>
                <div class="footer-desc">
                    <p>gustoinfosys@gmail.com</p>
                </div>
            </div>
            <div class="col-md-4 footer-col">
                <div class="footer-social-links">
                    <span class="social-icon">
                        <a href="#">
                            <i class="fa fa-facebook"></i>
                        </a>
                    </span>
                    <span class="social-icon">
                        <a href="#">
                            <i class="fa fa-twitter"></i>
                        </a>
                    </span>
                    <span class="social-icon">
                        <a href="#">
                            <i class="fa fa-linkedin"></i>
                        </a>
                    </span>
                </div>
            </div>
        </div>
	</div> <!-- / END CONTAINER -->
    
</footer> <!-- / END FOOOTER  -->

<footer id="footer-bottom">
    <div class="container">    
        <div class="row">
            <div class="col-md-12">
                <p>© <?php echo date('Y')?> Gusto Infosys Pvt. Ltd.</p>
            </div>
        </div>
    </div>
</footer>

	</div><!-- mobile-bg-fix-whole-site -->
</div><!-- .mobile-bg-fix-wrap -->


<?php wp_footer(); ?>

<?php zerif_bottom_body_trigger(); ?>

</body>

</html>
