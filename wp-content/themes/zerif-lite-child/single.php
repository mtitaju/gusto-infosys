<?php
/**
 * Template Name: All Post List
 *
 * @package zerif-lite
 */
global $post;
$post_id = $post->ID;
$post_category = get_the_category();
get_header();
?>

<div class="clear"></div>

</header> <!-- / END HOME SECTION  -->
<?php zerif_after_header_trigger(); ?>
<div id="top-banner-all-page" class="transparent-dark-bg">
    <div class="page-title-main-bg">
        <h1 class="text-center text-white page-title-main"><?php echo $post_category[0]->name;?></h1>
    </div>
</div>
<div id="content" class="site-content main-site-container inner-page-container">
    <div class="container">
        <?php
        switch ($post_category[0]->slug) {
            case 'blog':
                get_template_part( 'inner-pages/blog-single');
                break;
            case 'our-services':
                get_template_part( 'inner-pages/service-single');
                break;
            case 'portfolio':
                get_template_part( 'inner-pages/portfolio-single');
                break;
        }
        ?>
    </div>
</div>

<?php

get_footer();
?>