<?php
/**
 * Our Focus section
 *
 * @package zerif-lite
 */

zerif_before_our_focus_trigger();

$zerif_ourfocus_show = get_theme_mod( 'zerif_ourfocus_show' );

echo '<section class="focus ' . ( ( is_customize_preview() && ( ! isset( $zerif_ourfocus_show ) || $zerif_ourfocus_show == 1 ) ) ? ' zerif_hidden_if_not_customizer ' : '' ) . '" id="focus">';

?>

	<?php zerif_top_our_focus_trigger(); ?>

	<div class="container services-container">

		<!-- SECTION HEADER -->

		<div class="section-header services-header">

			<!-- SECTION TITLE AND SUBTITLE -->

			<h2>Our Services</h2>

		</div>

		<div class="row">

				<?php
				$args = array( 'numberposts' => 4, 'category_name' => 'our-services' );
				$services_post = get_posts( $args );
				if(is_array($services_post)){
					foreach ($services_post as $key => $service) {
						//echo '<pre>'; print_r($service); echo '</pre>';
						$service_metas = get_post_meta($service->ID);
						?>
						<div class="col-md-3 services-content">
							<div class="main-icon"><i class="<?php echo $service_metas['main-icon'][0] ?>"></i></div>
							<div class="title-lg service-title"><?php echo $service->post_title;?></div>
							<div class="desc-sm service-desc"><?php echo $service->post_excerpt;?></div>
							<div class="readmore">
								<a href="<?php the_permalink($service->ID);?>">
									Read More <i class="fa fa-arrow-circle-right"></i>
								</a>
							</div>
						</div>
						<?php
					}
					?>
					<div class="col-md-12">
						<div class="view-all-wrap">
							<a href="#" class="btn-clear btn-clear-white">
								View All
							</a>
						</div>
					</div>
					<?php
				}			

				?>
		</div>

	</div> <!-- / END CONTAINER -->

	<?php zerif_bottom_our_focus_trigger(); ?>

</section>  <!-- / END FOCUS SECTION -->

<?php zerif_after_our_focus_trigger(); ?>
