<?php
/**
 * Blog section
 *
 * @package zerif-lite
 */

echo '<section class="blog-section" id="blog">';

	echo '<div class="container">';

		echo '<div class="section-header blog-header">';

			/* Title */
			echo '<h2 class="text-white">Recent Blogs</h2>';

        echo '</div>';
        ?>
        
        <div class="row">
            <?php
            $args = array( 'numberposts' => 3, 'category_name' => 'blog' );
            $blog_list = get_posts( $args );
            if(is_array($blog_list)){
                $blog_page = get_page_by_path('blog');
                ?>
                <?php
                if(is_array($blog_list) && sizeof($blog_list)>0){
                   
                    foreach ($blog_list as $key => $blog) {
                        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $blog->ID ), 'single-post-thumbnail' );
                        
                        $blog_metas = get_post_meta($blog->ID);
                        ?>
                        <div class="col-md-4">
                            <div class="blog-information">
                                <div class="blog-image-wrap">
                                    <figure>
                                        <?php
                                        if(is_array($image)){
                                            ?>
                                            <img class="img-responsive" src="<?php echo $image[0]?>">
                                            <?php
                                        }else{
                                            ?>
                                            <span class="dummy-image"><i class="fa fa-picture-o"></i></span>
                                            <?php
                                        }
                                        ?>
                                    </figure>
                                </div>
                                <div class="blog-title"><?php echo $blog->post_title;?></div>
                                <div class="blog-content"><?php echo $blog->post_content;?></div>
                                <div class="view-all-wrap">
                                    <a href="#" class="btn-clear btn-clear-green">
                                        Read Article
                                    </a>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    
                }
                ?>
                
                <?php
            }			
            
            ?>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="view-all-wrap">
                    <a href="#" class="btn-clear btn-clear-white">
                        View All
                    </a>
                </div>
            </div>
        </div>
        <?php	

	echo '</div>';//container

echo '</section>';