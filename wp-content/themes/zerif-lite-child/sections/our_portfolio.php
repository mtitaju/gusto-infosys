<?php
/**
 * Our Portfolio section
 *
 */


echo '<section class="our-portfolio" id="portfolio">';

	echo '<div class="container">';

        echo '<div class="section-header portfolio-header">';
            echo '<h2>Portfolio</h2>';			

		echo '</div>';
        ?>

        <div class="row">
            <?php
            $args = array( 'numberposts' => 12, 'category_name' => 'portfolio' );
            $portfolio_list = get_posts( $args );
            if(is_array($portfolio_list)){
                $portfolio_page = get_page_by_path('portfolio');
                ?>
                <div class="col-md-12">
                    <?php
                    if(is_object($portfolio_page) && isset($portfolio_page->post_content)){
                        echo $portfolio_page->post_content;
                    }
                    ?>
                </div>
                <?php
                foreach ($portfolio_list as $key => $portfolio) {
                    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $portfolio->ID ), 'single-post-thumbnail' );
                    $portfolio_metas = get_post_meta($portfolio->ID);
                    ?>
                    <div class="col-md-3 portfolio-content">
                        <div class="portfolio-image-wrap">
                            <figure>
                                <img class="img-responsive" src="<?php echo $image[0]?>">
                            </figure>
                        </div>
                        <div class="title-md portfolio-title"><?php echo $portfolio->post_title;?></div>
                        <div class="desc-sm portfolio-desc"><?php echo $portfolio->post_excerpt;?></div>
                        <div class="readmore">
                            <a href="<?php the_permalink($portfolio->ID);?>">
                                Project Overview <i class="fa fa-angle-double-right"></i>
                            </a>
                        </div>
                    </div>
                    <?php
                }
                ?>
                <div class="col-md-12">
                    <div class="view-all-wrap">
                        <a href="#" class="btn-clear btn-clear-green">
                            View All
                        </a>
                    </div>
                </div>
                <?php
            }			

            ?>
        </div>
        <?php

	echo '</div>';//container

echo '</section>';