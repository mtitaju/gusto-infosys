<?php
/**
 * Testimonials section
 *
 * @package zerif-lite
 */

zerif_before_testimonials_trigger();

$zerif_testimonials_show = get_theme_mod( 'zerif_testimonials_show' );

echo '<section class="testimonial ' . ( ( is_customize_preview() && ( ! isset( $zerif_testimonials_show ) || $zerif_testimonials_show == 1 ) ) ? ' zerif_hidden_if_not_customizer ' : '' ) . '" id="testimonials">';

	zerif_top_testimonials_trigger();

	echo '<div class="container">';

		echo '<div class="section-header testimonial-header">';

			/* Title */
			echo '<h2>Testimonials</h2>';

        echo '</div>';
        ?>
        <div class="row">
            <?php
            $args = array( 'numberposts' => 3, 'category_name' => 'testimonials' );
            $testimonial_list = get_posts( $args );
            if(is_array($testimonial_list)){
                $testimonial_page = get_page_by_path('testimonial');
                ?>
                <div class="col-md-12">
                    <?php
                    if(is_object($testimonial_page) && isset($testimonial_page->post_content)){
                        echo $testimonial_page->post_content;
                    }
                    ?>
                </div>
                <?php
                if(is_array($testimonial_list) && sizeof($testimonial_list)>0){
                    ?>
                    <div id="testimonial-carousel" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <?php
                            foreach ($testimonial_list as $key => $testimonial) {
                                ?>
                                <li data-target="#testimonial-carousel" data-slide-to="<?php echo $key;?>" class="<?php echo $key==0?'active':'';?>"></li>
                                <?php
                            }
                            ?>
                        </ol>
                    
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <?php
                            foreach ($testimonial_list as $key => $testimonial) {
                                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $testimonial->ID ), 'single-post-thumbnail' );
                                
                                $testimonial_metas = get_post_meta($testimonial->ID);
                                ?>
                                <div class="item <?php echo $key==0?'active':'';?>">
                                    <div class="testimonial-information">
                                        <div class="testimonial-image-wrap">
                                            <figure>
                                                <?php
                                                if(is_array($image)){
                                                    ?>
                                                    <img class="img-responsive img-circle" src="<?php echo $image[0]?>">
                                                    <?php
                                                }else{
                                                    ?>
                                                    <span class="dummy-user"><i class="fa fa-user"></i></span>
                                                    <?php
                                                }
                                                ?>
                                            </figure>
                                        </div>
                                        <div class="tesmtimonial-title"><?php echo $testimonial->post_title;?></div>
                                        <div class="tesmtimonial-org"><?php echo $testimonial->post_excerpt;?></div>
                                        <div class="testimonial-content"><?php echo $testimonial->post_content;?></div>
                                    </div>
                                    <div class="carousel-caption">
                                    </div>
                                </div>
                            <?php
                            }
                            ?>
                        </div>
                    
                        <!-- Controls -->
                        <a class="left carousel-control" href="#testimonial-carousel" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"><i class="fa fa-angle-left"></i></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#testimonial-carousel" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"><i class="fa fa-angle-right"></i></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>        
                    
                    <?php
        
                }
                ?>
                
                <?php
            }			

            ?>
        </div>

        <?php	

	echo '</div>';//container

	zerif_bottom_testimonials_trigger();

echo '</section>';

zerif_after_testimonials_trigger();