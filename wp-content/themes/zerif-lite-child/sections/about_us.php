<?php
/**
 * About us section
 *
 * @package zerif-lite
 */

$about_page = get_page_by_path('about');
//echo '<div>'; print_r($about_page); echo '</div>';

zerif_before_about_us_trigger();

$zerif_aboutus_show = get_theme_mod( 'zerif_aboutus_show' );

echo '<section class="about-us ' . ( ( is_customize_preview() && ( ! isset( $zerif_aboutus_show ) || $zerif_aboutus_show == 1 ) ) ? ' zerif_hidden_if_not_customizer ' : '' ) . '" id="aboutus">';

?>

	<?php zerif_top_about_us_trigger(); ?>

	<div class="container about-us-container">

		<!-- SECTION HEADER -->

		<div class="section-header about-us-header">
            <?php
            if(is_object($about_page) && isset($about_page->post_title)){
                ?>
                <h2><?php echo $about_page->post_title;?></h2>
                <?php
            }
            ?>
            

		</div><!-- / END SECTION HEADER -->

		<!-- 3 COLUMNS OF ABOUT US-->

		<div class="row">

			<!-- COLUMN 1 - BIG MESSAGE ABOUT THE COMPANY-->
            <?php
            if(is_object($about_page) && isset($about_page->post_content)){
                //echo '<pre>'; print_r($about_page); echo '</pre>';
                ?>
                <div class="col-lg-12 col-md-12 about-us-content">
                    <?php
                    echo $about_page->post_content;
                    //echo get_the_excerpt(100);
                    ?>
                    <div class="readmore">
                        <a href="<?php echo get_page_link($about_page->ID);?>">
                            Read More <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div>               
                <?php
            }
            ?>


	    </div> <!-- / END 3 COLUMNS OF ABOUT US-->

	</div> <!-- / END CONTAINER -->

	<?php zerif_bottom_about_us_trigger(); ?>

</section> <!-- END ABOUT US SECTION -->

<?php zerif_after_about_us_trigger(); ?>
