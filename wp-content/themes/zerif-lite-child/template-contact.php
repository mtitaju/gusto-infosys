<?php
/**
 * Template Name: Contact Template
 *
 * @package zerif-lite
 */
global $post;
$post_slug = $post->post_name;
$about_page = get_page_by_path('contact');
$post_id = $about_page->ID;
$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'single-post-thumbnail' );
get_header();
?>

<div class="clear"></div>

</header> <!-- / END HOME SECTION  -->
<?php zerif_after_header_trigger(); ?>
<div id="top-banner-all-page" class="transparent-dark-bg">
    <div class="page-title-main-bg">
        <h1 class="text-center text-white page-title-main"><?php echo get_the_title();?></h1>
    </div>
</div>  
<div id="content" class="site-content main-site-container" >
    <div class="contact-container">
        <div class="container-fluid">
            <?php
            get_template_part( 'inner-pages/contact');
            ?>
        </div>
    </div>
</div>

<?php

get_footer();
?>