<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'gusto' );

/** MySQL database username */
define( 'DB_USER', 'milan' );

/** MySQL database password */
define( 'DB_PASSWORD', 'milan' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'pD_AW}nDD{|(6xpt9zt9d~rCbyE#>-al]if@WN?#j@0Da#&r%;V6oBJS!H/s.m!3' );
define( 'SECURE_AUTH_KEY',  'up3A*Rh)rm5=z9oI)D=eKFtf!x-G?rR1RuC-F Jlj}2zje3)J0bj,TVU66XzO0.~' );
define( 'LOGGED_IN_KEY',    'e!K_UkNYBTOLxr?01IjXDP8Gd[=Ah(Zn405YKv:0R|$HA2B&K=}[48MVd|`-Az[i' );
define( 'NONCE_KEY',        'eq@$L1|WKM;f}>t&?V)[I/P;1w[q&VEae{itjrV20J(8Cz!YRzZqv`#e{%-<$GqJ' );
define( 'AUTH_SALT',        'I64{T3%VGo3V2Tl0Gl4b7v&v2>nH~e0#K?9c`O<o6.mBX6N_yw _+3yo(%3Mr#?p' );
define( 'SECURE_AUTH_SALT', 'EJ1C1I7hQAW[ Z#_z3eb!)&Xv6T-$LLazIN<hP,0/oTOY99=i+8IzIvfA>$sY+A^' );
define( 'LOGGED_IN_SALT',   'p5IXGBbY4PVa/gTC%t?g!Wx1^E6ZjHYkN>E4x2|kMD{V/lm#J?;Vi@6X]uj{]A;0' );
define( 'NONCE_SALT',       '|+6Lx-CzD?pJ9Q#QegpMbC~&B^zX8TZ[O<1y{X.8V#N@`WMy&?H2t__c7`R85WuM' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
